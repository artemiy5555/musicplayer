public class SongTable {

    private String song;

    private String artist;

    private String genge;

    public SongTable(String song, String artist, String genge) {
        this.song = song;
        this.artist = artist;
        this.genge = genge;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenge() {
        return genge;
    }

    public void setGenge(String genge) {
        this.genge = genge;
    }

    @Override
    public String toString() {
        return "SongTable{" +
                "song='" + song + '\'' +
                ", artist='" + artist + '\'' +
                ", genge='" + genge + '\'' +
                '}';
    }
}
